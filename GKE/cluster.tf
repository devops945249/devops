resource "google_container_cluster"  "gkecluster"  {
    name = "gkecluster1"
    location =  "asia-east1"
    initial_node_count = 1 #incase of default node pool
    networking_mode = "VPC_NATIVE"
    network = google_compute_network.gkenetwork.self_link
    subnetwork = google_compute_subnetwork.gkesubnetwork.self_link

    workload_identity_config {
        workload_pool = "airy-charge-406010.svc.id.goog"
    }

    ip_allocation_policy {
        cluster_secondary_range_name = "k8s-pod-range"
        services_secondary_range_name = "k8s-service-range"
    }
}
