resource "google_compute_network" "gkenetwork" {
    name =  "gke-network"
    auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "gkesubnetwork" {
    name = "gke-subnetwork"
    region = "asia-east1"
    ip_cidr_range = "10.0.1.0/24"
    network = google_compute_network.gkenetwork.id

     secondary_ip_range {
    range_name    = "k8s-pod-range"
    ip_cidr_range = "10.48.0.0/14"
  }
  secondary_ip_range {
    range_name    = "k8s-service-range"
    ip_cidr_range = "10.52.0.0/20"
  }

}

# Create Cloud Router

resource "google_compute_router"  "gkecloudrouter" {
    name = "gke-cloud-router"
    region = "asia-east1"
    network =  google_compute_network.gkenetwork.id
}

