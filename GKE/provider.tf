terraform {
    required_providers {
        google = {
             source = "hashicorp/google"
            version = "5.8.0"
        }
    }
}

provider "google" {
    project = "airy-charge-406010"
}