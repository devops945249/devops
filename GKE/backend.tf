terraform {
    backend "gcs" {
        bucket = "terraformstatebucketal"
        prefix = "terraform/state"
    }
}
